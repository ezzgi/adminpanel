import { DashboardComponent } from './../../dashboard/dashboard.component';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { DefaultLayoutComponent } from 'src/app/containers';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private router: Router) {}
  linkDashboard(){
    //this.router.navigateByUrl('#/dashboard');
    window.location.href = '#/dashboard';
  }
}
